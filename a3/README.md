> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Application Development

## Enrique Toledo

### Assignment 3 Requirements:

*Three parts:*

1. Create local repository
2. forward engineer local repository 
3. Create and populate tables for customer, petstore, & pet

#### README.md file should include the following items:

* Screenshot of ERD
* Screenshot of a3.index.jsp
* Links to a3.mwb & a3.sql
* Bitbucket repo link for this assignment and bitbucketstationlocations


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

#### Assignment Screenshots:

*Screenshot of A3 index.jsp*:

![http://localhost:9999/a3/index](img/a3index.png)


*Screenshot of A3 ERD*:

![A3 ERD](img/a3.png)

*A3 docs: a3.mwb and a3.sql*:
[A3 MWB File](docs/a3.mwb "A3 ERD in .mwb format")
[A3 SQL File](docs/a3.sql "A3 SQL Script")

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/EnriqueToledo/bitbucketstationlocations/src/master/ "Bitbucket Station Locations")