> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS4368 - Advanced Web Applications

## Enrique Toledo

### LIS4368 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Install Tomcat
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorials (bitbucketstationlocations and myteamquotes)
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Create Hello-World Java Servlet
    - Create a database servlet
    - Provide screenshots of funcitonal links
    
3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create local repository
    - forward engineer local repository 
    - Create and populate tables for customer, petstore, & pet

4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Create basic server-side validation
    - modify thanks.jsp
    - modify customerform.jsp
    - modify customerservlet.java

5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Create basic server-side validation
    - Create ConnectionPool.java
    - Create Customer.java
    - Create CustomerDB.java

6. [P1 README.md](p1/README.md "My P1 README.md file")
    - Modify index.jsp
    - Pass bootstrap client-side validation
    - customize splashpage carousel

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Provide create, read, update, and delete (CRUD) funcionality to servlet web application
    - Valid user form entry
    - Display updated MYSQL customer table
    - Update MYSQL customer table
