> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Application Development

## Enrique Toledo

### Assignment 2 Requirements:

*Three parts:*

1. Create Hello-World Java Servlet
2. Create a database servlet
3. Provide screenshots of funcitonal links

#### README.md file should include the following items:

* Screenshot of assessment links
* One screenshot of query results from 
* Link to local lis4368 web app: [http://localhost:9999/hello/querybook.html](http://localhost:9999/hello/querybook.html "http://localhost:9999/hello/querybook.html")
* Bitbucket repo link for this assignment and bitbucketstationlocations


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

#### Assignment Screenshots:

*Screenshot of A2 index.jsp*:

![http://localhost:9999/a2/index](img/a2index.png)


*Screenshot of running http://localhost:9999/hello assessment link screenshot*:

![http://localhost:9999/hello](img/hello.png)

*Screenshot of running http://localhost:9999/hello/HelloHome.html assessment link screenshot*:

![http://localhost:9999/hello/HelloHome.html](img/index.png)

*Screenshot of http://localhost:9999/hello/sayhello assessment link screenshot*:

![http://localhost:9999/hello/sayhello](img/sayHello.png)

*Screenshot of http://localhost:9999/hello/querybook.html assessment link screenshot*:

![http://localhost:9999/hello/querybook.html](img/querybook.png)

*Screenshot of query result*:

![query result](img/queryresults.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/EnriqueToledo/bitbucketstationlocations/src/master/ "Bitbucket Station Locations")