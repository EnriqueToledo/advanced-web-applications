> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Application Development

## Enrique Toledo

### Assignment 1 Requirements:

*Three parts:*

1. Distributed Version Control with Git and Bitbucket
2. Java/SP/Servlet Development Installation
3. Chapter Questions (Chs 1 - 4)

#### README.md file should include the following items:

* Screenshot of running java Hello (#1 above)
* Screenshot of running Https://localhost:9999 (#2 above, Step #4(b) in tutorial)
* Link to local lis4368 web app: [http://localhost:9999/lis4368](http://localhost:9999/lis4368 "http://localhost:9999/lis4368")
* Bitbucket repo link for this assignment and bitbucketstationlocations


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - Create an empty Git repository or reinitialize an existing one.
2. git status - Show the working tree status.
3. git add - Add file content to the index.
4. git commit - Record changes to the repository.
5. git push - Update remote refs along with associated objects.
6. git pull - Fetch from and integrate with another repository or a local branch.
7. git merge - Join two or more development histories together.

#### Assignment Screenshots:

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk.png)

*Screenshot of running https://localhost:9999*:

![tomcat Screenshot](img/tomcat.png)

*Screenshot of A1 index.jsp*:

![A1 index.jsp](img/a1_index.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/EnriqueToledo/bitbucketstationlocations/src/master/ "Bitbucket Station Locations")