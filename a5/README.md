> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Application Development

## Enrique Toledo

### Assignment 5 Requirements:

*Four parts:*

1. Create basic server-side validation
2. Create ConnectionPool.java
3. Create Customer.java
4. Create CustomerDB.java

#### README.md file should include the following items:

* Screenshot of Valid User Enrty form
* Screenshot of Passed Validation
* Screenshots of skill sets 10 - 12


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

| *Screenshot of Valid User Form Entry:* | *Screenshot of SS11:* |
|----------------------------------------|-----------------------|
| ![Valid validation](img/valid.png)      | ![passed validation](img/pic2.png) |

| *Screenshot of Associated Database Entry:* |                            |
|--------------------------------------------|----------------------------|
| ![1](img/associated_1.png)                 | ![2](img/associated_2.png) |

