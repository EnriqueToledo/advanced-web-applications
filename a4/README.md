> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Application Development

## Enrique Toledo

### Assignment 4 Requirements:

*Four parts:*

1. Create basic server-side validation
2. modify thanks.jsp
3. modify customerform.jsp
4. modify customerservlet.java

#### README.md file should include the following items:

* Screenshot of Failed Validation
* Screenshot of Passed Validation
* Screenshots of skill sets 10 - 12


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshot of Failed Validation:*
![failed validation](img/failed.png)

*Screenshot of Passed Validation:*
![passed validation](img/passed.png)

| *Screenshot of SS10:* | *Screenshot of SS11:* |
|-----------------------|-----------------------|
| ![ss10](img/ss10.png) | ![ss11](img/ss11.png) |

