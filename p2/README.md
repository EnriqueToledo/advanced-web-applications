> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Application Development

## Enrique Toledo

### Project 2 Requirements:

*Four parts:*

1. Provide create, read, update, and delete (CRUD) funcionality to servlet web application
2. id user form entry
3. Display updated MYSQL customer table
4. Update MYSQL customer table

#### README.md file should include the following items:

* Screenshot of Valid User Form Entry
* Screenshot of P2 passed validation
* Screenshot of Display customer data
* Screenshot of Modify form
* Screenshot of modified data
* Screenshot of Delete Warning
* Screenshot of associated database changes

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

#### Assignment Screenshots:
 
| Valid User Form Entry                                     | Passed Validation                              |
|-----------------------------------------------------------|------------------------------------------------|
| ![Valid User Form Entry](img/ValidUserFormEntry.png)      | ![Passed Validation](img/PassedValidation.png) |

| Display Data                         | Modify data                    |
|--------------------------------------|--------------------------------|
| ![Display Data](img/DisplayData.png) | ![Modify data](img/Modify.png) |

| Modified data                      | Delete Warning                           |
|------------------------------------|------------------------------------------|
| ![Modified data](img/Modified.png) | ![Delete Warning](img/DeleteWarning.png) |

| Associated Database Changes       |
|-----------------------------------|
| ![associated](img/associated.png) |
