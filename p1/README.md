> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Application Development

## Enrique Toledo

### Assignment 3 Requirements:

*Three parts:*

1. Modify index.jsp
2. Pass bootstrap client-side validation
3. customize splashpage carousel

#### README.md file should include the following items:

* Screenshot of Splash Screen
* Screenshot of p1 failed validation
* Screenshot of passed client side validation

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

#### Assignment Screenshots:

*Screenshot of Main/Splash Page*:

![main/spash page](img/splash.png)
 
| Passed Validation                    | Failed Validation                    |
|--------------------------------------|--------------------------------------|
| ![Passed Validation](img/passed.png) | ![Failed Validation](img/failed.png) |